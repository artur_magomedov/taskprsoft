//
//  RealmModel.swift
//  taskProSoft
//
//  Created by Артур on 13.02.2022.
//

import RealmSwift

class TaskRealmModel: Object {
    @Persisted var task_time = ""
    @Persisted var task_date = ""
    @Persisted var name: String = ""
    @Persisted var descriptionString: String = ""
}
