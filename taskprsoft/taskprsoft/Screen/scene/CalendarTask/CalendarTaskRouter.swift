//
//  CalendarTaskRouter.swift
//  taskProSoft
//
//  Created by Артур on 02.02.2022.
//

import Foundation
import UIKit

class CalendarTaskRouter {
    //MARK: - internal property
    var viewController: UIViewController?
    var dataStore: CalendarTaskDataStore?
}

extension CalendarTaskRouter: CalendarTaskRoutingLogic, CalendarDataPassing {
    func routeTo() {
        DispatchQueue.main.async {
            self.viewController?.navigationController?.pushViewController(ToDoListViewController(), animated: true)
        }
    }
}
