//
//  CalendarTaskProtocol.swift
//  taskProSoft
//
//  Created by Артур on 02.02.2022.
//

import Foundation
import UIKit


protocol CalendarTaskBusinessLogic: AnyObject {
    func fetchData()
}

protocol CalendarTaskPresentationLogic: AnyObject {
    func presetData(arra: [CalendarTask])
}

protocol CalendarTaskDisplayLogic: AnyObject {
    func displayData(arra: [CalendarTask])
}

protocol CalendarTaskRoutingLogic: AnyObject {
    func routeTo()
}

protocol CalendarDataPassing {
    var dataStore: CalendarTaskDataStore? {get set}
}

protocol CalendarTaskDataStore {
    var models: [CalendarTask]? {get set}
}


