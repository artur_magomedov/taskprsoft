//
//  CalendarTaskPresenter.swift
//  taskProSoft
//
//  Created by Артур on 02.02.2022.
//

import Foundation
import UIKit

class CalendarTaskPresenter {
    //MARK: - internal view
    var viewController: CalendarTaskDisplayLogic?
    let dateFormater = DateFormatter()
}

extension CalendarTaskPresenter: CalendarTaskPresentationLogic {
    func presetData(arra: [CalendarTask]) {
      var arrayNew = [CalendarTask]()
       var array = arra.map { item in
           dateFormater.locale = NSLocale.current
           dateFormater.dateFormat = "HH:mm"
           arrayNew.append(CalendarTask(date_start: self.dateFormater.string(from: Date(timeIntervalSince1970: Double(item.date_start)!)), date_finish: self.dateFormater.string(from: Date(timeIntervalSince1970: Double(item.date_finish)!)), description: item.description, name: item.name))
        }
        
        viewController?.displayData(arra: arrayNew)
    }
}
