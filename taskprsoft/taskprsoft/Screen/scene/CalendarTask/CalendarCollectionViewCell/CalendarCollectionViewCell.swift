//
//  CalendarCollectionViewCell.swift
//  taskProSoft
//
//  Created by Артур on 12.02.2022.
//

import UIKit

class CalendarCollectionViewCell: UICollectionViewCell {
    
    var calendLab: UILabel = {
        let month = UILabel()
        month.textColor = .black
        month.font = UIFont.systemFont(ofSize: 15)
       
        return month
    }()
    var monthLab: UILabel = {
        let month = UILabel()
        month.textColor = .black
        month.font = UIFont.systemFont(ofSize: 23)
        return month
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.backgroundColor = .white
        contentView.layer.shadowRadius = 2
        contentView.layer.shadowOffset = CGSize(width: 2, height: 6)
        contentView.layer.shadowColor = UIColor.black.cgColor
        contentView.layer.shadowOpacity = 3
        contentView.layer.borderColor = UIColor.separator.cgColor
        contentView.layer.borderWidth = 2
        contentView.layer.cornerRadius = 20
        contentView.layer.backgroundColor = UIColor.white.cgColor
        
        contentView.addSubview(monthLab)
        contentView.addSubview(calendLab)
        monthLab.snp.makeConstraints({ make in
            make.centerX.equalTo(25)
            make.width.equalToSuperview().multipliedBy(0.6)
            make.height.equalToSuperview().multipliedBy(0.6)
            make.bottom.equalToSuperview().multipliedBy(1.1)
        })
    }
    
    func setup(calendarLabel: String, monthLab: String ){
        self.calendLab.text = calendarLabel
        self.monthLab.text = monthLab
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   
}
