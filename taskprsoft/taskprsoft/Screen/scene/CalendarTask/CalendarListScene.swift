//
//  CalendarListScene.swift
//  taskProSoft
//
//  Created by Артур on 02.02.2022.
//

import UIKit

enum CalendarScene {
  static func build() -> UIViewController {
    let viewController = CalendarTaskViewController()
    let interactor = CalendarTaskInteractor()
    let presenter = CalendarTaskPresenter()
    let router = CalendarTaskRouter()
      
    
    viewController.router = router
    router.viewController = viewController
    viewController.interactor = interactor
    interactor.presenter = presenter
    presenter.viewController = viewController
    router.dataStore = interactor
        
    return viewController
    }
}
