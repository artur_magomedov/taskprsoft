//
//  CalendarTableViewCell.swift
//  taskProSoft
//
//  Created by Артур on 02.02.2022.
//

import UIKit

class CalendarTableViewCell: UITableViewCell {

    var data: UILabel = {
        let text = UILabel()
        text.frame = CGRect(x: 75, y: 15, width: 250, height: 35)
        text.layer.borderWidth = 2
        text.textAlignment = .center
        text.layer.cornerRadius = 10
        text.layer.shadowRadius = 4
        text.layer.shadowOffset = CGSize(width: 3, height: 5)
        text.layer.shadowColor = UIColor.black.cgColor
        text.layer.shadowOpacity = 2
        text.font = UIFont.systemFont(ofSize: 22)
        text.layer.backgroundColor = UIColor.opaqueSeparator.cgColor
        text.layer.borderColor = UIColor.separator.cgColor
        return text
    }()
    
   
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.translatesAutoresizingMaskIntoConstraints = false
         contentView.layer.shadowRadius = 8
         contentView.layer.shadowOffset = CGSize(width: 10, height: 15)
         contentView.layer.shadowColor = UIColor.black.cgColor
         contentView.layer.shadowOpacity = 3
         contentView.layer.borderColor = UIColor.separator.cgColor
         contentView.layer.borderWidth = 5
         contentView.layer.cornerRadius = 10
         contentView.layer.backgroundColor = UIColor.white.cgColor
        
        contentView.addSubview(data)
      
    }
    
    func setup(text: String) {
        self.data.text = text
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
