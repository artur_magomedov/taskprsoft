//
//  CalendarTaskInteractor.swift
//  taskProSoft
//
//  Created by Артур on 02.02.2022.
//

import UIKit
import Foundation
import RealmSwift

class CalendarTaskInteractor: CalendarTaskBusinessLogic, CalendarTaskDataStore  {

    //MARK: - internal properties
    var presenter: CalendarTaskPresentationLogic?
    var models: [CalendarTask]?
    //MARK: - fetchData
    func fetchData() {
        guard let path = Bundle.main.path(forResource: "task", ofType: "json") else {return}
            let url = URL(fileURLWithPath: path)
        do {
        
            let data = try Data(contentsOf: url)
            
            models = try JSONDecoder().decode([CalendarTask].self, from: data)
            
        } catch {
            print("\(error.localizedDescription)")
        
        }
        if models != nil {
            presenter?.presetData(arra: models!)
        } else {
            print("error models")
        }
        
    }

}
