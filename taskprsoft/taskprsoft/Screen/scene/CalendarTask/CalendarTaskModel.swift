//
//  CalendarTaskModel.swift
//  taskProSoft
//
//  Created by Артур on 02.02.2022.
//

import UIKit
import Foundation

// MARK: - CalendarTask
struct CalendarTask: Codable {
    let date_start: String
    let date_finish: String
    let description: [String]
    let name: String
}
