//
//  CalendarTaskViewController.swift
//  taskProSoft
//
//  Created by Артур on 02.02.2022.
//

import UIKit
import SnapKit
import Foundation
import RealmSwift

var selectedDate = Date()

class CalendarTaskViewController: UIViewController, UICollectionViewDelegateFlowLayout {
    //MARK: - internal properties
    var tableView = UITableView()
    var calendarCollection: UICollectionView?
    var stackView = UIStackView()
    var interactor: CalendarTaskBusinessLogic?
    var router: CalendarTaskRoutingLogic?
    var identifire = "cell"
    var arrays = [CalendarTask]()
    var totalSquares = [Date]()
    var totalSquar = [String]()
   
    //MARK: - buttonLeft
    let buttonLeft: UIButton = {
        let but = UIButton()
        but.setImage(UIImage(systemName: "arrowshape.turn.up.left"), for: .normal)
        but.setImage(UIImage(systemName: "arrowshape.turn.up.left.fill"), for: .highlighted)
        but.setTitleColor(.black, for: .normal)
        but.setTitleColor(.red, for: .highlighted)
        but.frame = CGRect(x: 10, y: 50, width: 150, height: 80)
        but.addTarget(self, action: #selector(nextWeak), for: .touchUpInside)
        return but
    }()
    //MARK: - buttonRight
    let buttonRight: UIButton = {
        let but = UIButton()
        but.setImage(UIImage(systemName: "arrowshape.turn.up.right"), for: .normal)
        but.setImage(UIImage(systemName: "arrowshape.turn.up.right.fill"), for: .highlighted)
        but.setTitleColor(.black, for: .normal)
        but.setTitleColor(.red, for: .highlighted)
        but.frame = CGRect(x: 260, y: 50, width: 150, height: 80)
        but.addTarget(self, action: #selector(previousWeek), for: .touchUpInside)
        return but
    }()
    //MARK: - monthLab
    var monthLab: UILabel = {
        let month = UILabel()
        month.frame = CGRect(x: 150, y: 65, width: 150, height: 50)
        month.font = UIFont.systemFont(ofSize: 20)
        month.textColor = .blue
        return month
    }()
    //MARK: - sun
    var sun: UILabel = {
        let month = UILabel()
        month.frame = CGRect(x: 10, y: 11, width: 100, height: 20)
        month.font = UIFont.systemFont(ofSize: 26)
        month.text = "Sun"
        month.textColor = .systemGray
        return month
    }()
    //MARK: - mon
    var mon: UILabel = {
        let month = UILabel()
        month.frame = CGRect(x: 70, y: 11, width: 100, height: 20)
        month.font = UIFont.systemFont(ofSize: 26)
        month.text = "Mon"
        month.textColor = .systemGray
        return month
    }()
    //MARK: - tue
    var tue: UILabel = {
        let month = UILabel()
        month.frame = CGRect(x: 130, y: 11, width: 100, height: 20)
        month.font = UIFont.systemFont(ofSize: 26)
        month.text = "Tue"
        month.textColor = .systemGray
        return month
    }()
    //MARK: - wed
    var wed: UILabel = {
        let month = UILabel()
        month.frame = CGRect(x: 185, y: 11, width: 100, height: 20)
        month.font = UIFont.systemFont(ofSize: 26)
        month.text = "Wed"
        month.textColor = .systemGray
        return month
    }()
    //MARK: - thur
    var thur: UILabel = {
        let month = UILabel()
        month.frame = CGRect(x: 250, y: 11, width: 100, height: 20)
        month.font = UIFont.systemFont(ofSize: 26)
        month.text = "Thur"
        month.textColor = .systemGray
        return month
    }()
    //MARK: - fri
    var fri: UILabel = {
        let month = UILabel()
        month.frame = CGRect(x: 320, y: 11, width: 100, height: 20)
        month.font = UIFont.systemFont(ofSize: 26)
        month.text = "Fri"
        month.textColor = .systemGray
        return month
    }()
    //MARK: - sat
    var sat: UILabel = {
        let month = UILabel()
        month.frame = CGRect(x: 370, y: 11, width: 100, height: 20)
        month.font = UIFont.systemFont(ofSize: 26)
        month.text = "Sat"
        month.textColor = .systemGray
        return month
    }()
    //MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        print(Realm.Configuration.defaultConfiguration.fileURL)
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "plus"), style: .plain, target: self, action: #selector(saveTask))
        view.backgroundColor = .systemMint
        view.addSubview(monthLab)
        view.addSubview(stackView)
        view.addSubview(buttonLeft)
        view.addSubview(buttonRight)
        stackView.addSubview(sun)
        stackView.addSubview(mon)
        stackView.addSubview(tue)
        stackView.addSubview(wed)
        stackView.addSubview(thur)
        stackView.addSubview(fri)
        stackView.addSubview(sat)
       
        
        interactor?.fetchData()
        
        view.addSubview(tableView)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        
        tableView.rowHeight = 70
        tableView.frame = CGRect(x: 0, y: 500, width: 430, height: 550)
        
        stackView.backgroundColor = .white
        stackView.frame = CGRect(x: 0, y: 120, width: 428, height: 50)
       
        
        setUpContactsCollectionView()
        
        setMonthView()
        setsCellsView()
        
        setWeekView()
        
    }
    
    override open var shouldAutorotate: Bool{
        return false
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        tableView.reloadData()
    }
    //MARK: - saveTask
   @objc func saveTask() {
        navigationController?.pushViewController(ToDoListViewController(), animated: true)
    }
    //MARK: - previousWeek
    @objc func previousWeek() {
        selectedDate = CalendarSetting().addDays(date: selectedDate, days: 7)
        setWeekView()
    }
    //MARK: - nextWeak
    @objc func nextWeak() {
        selectedDate = CalendarSetting().addDays(date: selectedDate, days: -7)
        setWeekView()
    }
    
    // MARK: - Config collection view
    func setUpContactsCollectionView() {
            let layout = UICollectionViewFlowLayout()
        calendarCollection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        calendarCollection!.dataSource = self
        calendarCollection!.delegate = self
        calendarCollection!.translatesAutoresizingMaskIntoConstraints = false
        calendarCollection?.frame = CGRect(x: 0, y: 160, width: 428, height: 340)
        calendarCollection!.register(CalendarCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        view.addSubview(calendarCollection!)
    }
    
    //MARK: - setWeekView
    func setWeekView()
    {
        totalSquares.removeAll()
        
        var current = CalendarSetting().sundayForDate(date: selectedDate)
        let nextSunday = CalendarSetting().addDays(date: current, days: 7)
        
        while (current < nextSunday)
        {
            totalSquares.append(current)
            current = CalendarSetting().addDays(date: current, days: 1)
        }
        
        monthLab.text = CalendarSetting().monthString(date: selectedDate)
            + " " + CalendarSetting().yearString(date: selectedDate)
        calendarCollection!.reloadData()
        tableView.reloadData()
    }
    //MARK: - setsCellsView
    func setsCellsView() {
        let width = (calendarCollection!.frame.size.width - 2) / 8
        let height = (calendarCollection!.frame.size.height - 2) / 8
        
        let flowLayout = calendarCollection!.collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.itemSize = CGSize(width: width, height: height)
    }
    //MARK: - setMonthView
    func setMonthView() {
        totalSquar.removeAll()
        
        let daysInMonth = CalendarSetting().daysInMonth(date: selectedDate)
        let firstDayOfMonth = CalendarSetting().firstOfMonth(date: selectedDate)
        let startingSpaces = CalendarSetting().weekDay(date: firstDayOfMonth)
        
        var count: Int = 1
        
        while(count <= 42) {
            if(count <= startingSpaces || count - startingSpaces > daysInMonth)
            {
                totalSquar.append("")
            }
            else
            {
                totalSquar.append(String(count - startingSpaces))
            }
            count += 1
        }
        monthLab.text = CalendarSetting().monthString(date: selectedDate)
            + " " + CalendarSetting().yearString(date: selectedDate)
        calendarCollection!.reloadData()
    }
}
//MARK: - extension UITableViewDelegate, UITableViewDataSource, CalendarTaskDisplayLogic
extension CalendarTaskViewController: UITableViewDelegate, UITableViewDataSource, CalendarTaskDisplayLogic {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Event().eventsForDate(date: selectedDate).count
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = CalendarTableViewCell.init(style: .default, reuseIdentifier: "cellId") as! CalendarTableViewCell
        
        var event = Event().eventsForDate(date: selectedDate)[indexPath.row]
        
        cell.setup(text: event.name + " " + CalendarSetting().timeString(date: event.date))
        
        
        return cell
    }
    
    //MARK: - load data view
    func displayData(arra: [CalendarTask]) {
        arrays.append(contentsOf: arra)
    }
    
}
//MARK: - extension UICollectionViewDelegate, UICollectionViewDataSource
extension CalendarTaskViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    // MARK: - Collection view methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.totalSquares.count
    }
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CalendarCollectionViewCell
       
        let date = totalSquares[indexPath.item]
        
        cell.monthLab.text = String(CalendarSetting().dayOfMonth(date: date))
        
        if(date == selectedDate)
        {
            cell.backgroundColor = UIColor.systemBlue
        }
        else
        {
            cell.backgroundColor = UIColor.white
        }
        
        return cell
    }
  
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedDate = totalSquares[indexPath.item]
        collectionView.reloadData()
        tableView.reloadData()
    }
}
