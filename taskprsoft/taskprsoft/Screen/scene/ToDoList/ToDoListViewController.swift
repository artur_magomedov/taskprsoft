//
//  ToDoListViewController.swift
//  taskProSoft
//
//  Created by Артур on 02.02.2022.
//

import UIKit

class ToDoListViewController: UIViewController {
    //MARK: - nameTF
    var nameTF: UITextField = {
        let nameTf = UITextField()
        nameTf.frame = CGRect(x: 60, y: 250, width: 300, height: 50)
        nameTf.placeholder = "text text"
        nameTf.layer.shadowRadius = 15
        nameTf.layer.shadowOffset = CGSize(width: 10, height: 20)
        nameTf.layer.shadowColor = UIColor.black.cgColor
        nameTf.layer.shadowOpacity = 3
        nameTf.layer.borderColor = UIColor.separator.cgColor
        nameTf.layer.borderWidth = 1
        nameTf.layer.cornerRadius = 4
        nameTf.layer.backgroundColor = UIColor.white.cgColor
        nameTf.borderStyle = .roundedRect
        return nameTf
    }()
    //MARK: - datePicker
    var datePicker: UIDatePicker = {
        let dataPick = UIDatePicker()
        dataPick.frame = CGRect(x: 60, y: 200, width: 90, height: 39)
        dataPick.datePickerMode = .time
        dataPick.layer.shadowRadius = 10
        dataPick.layer.shadowOffset = CGSize(width: 10, height: 20)
        dataPick.layer.shadowColor = UIColor.black.cgColor
        dataPick.layer.shadowOpacity = 3
        dataPick.layer.borderColor = UIColor.separator.cgColor
        dataPick.layer.borderWidth = 1
        dataPick.layer.cornerRadius = 4
        dataPick.layer.backgroundColor = UIColor.white.cgColor
        return dataPick
    }()
    //MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "plus"), style: .plain, target: self, action: #selector(saveAction))
        
        view.backgroundColor = .white
        view.addSubview(nameTF)
        view.addSubview(datePicker)
        
        datePicker.date = selectedDate
        
    }
    //MARK: - saveAction
    @objc func saveAction() {
        let newEvent = Event()
        newEvent.id = eventsList.count
        newEvent.name = nameTF.text!
        newEvent.date = datePicker.date
        eventsList.append(newEvent)
        navigationController?.popViewController(animated: true)
    }

}

//MARK: - extension UITextFieldDelegate
extension ToDoListViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
