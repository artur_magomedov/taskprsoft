//
//  DefaultUser.swift
//  testProjectForDT
//
//  Created by Александр Григоренко on 25.01.2022.
//

import Foundation

class DefaultUserService {
    // MARK: - authorization check
    private let signInStatusKey = "signInStatusKey"
    lazy var signInStatus: Bool = {
        return getBool(key: signInStatusKey) ?? false
    }()

    func didSignIn(completion: (() -> Void)?) {
        setValue(true, key: signInStatusKey)
        completion?()
    }

    // MARK: - Work with UserDefaults
        private func setValue(_ value: Any?, key: String) {
            UserDefaults.standard.set(value, forKey: key)
            UserDefaults.standard.synchronize()
        }

        private func getString(key: String) -> String? {
            let string = UserDefaults.standard.string(forKey: key)
            UserDefaults.standard.synchronize()
            return string
        }

        private func getBool(key: String) -> Bool? {
            let bool = UserDefaults.standard.bool(forKey: key)
            UserDefaults.standard.synchronize()
            return bool
        }
}
