//
//  AppDelegate.swift
//  taskprsoft
//
//  Created by Артур on 16.02.2022.
//

import UIKit


@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(
           _ application: UIApplication,
           didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
           setupStartVC()
           return true
       }
}

extension AppDelegate {
    private func setupStartVC() {
        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window
        changeRootVC()
    }

    public func changeRootVC() {
        if DefaultUserService().signInStatus {
            self.window?.rootViewController = UINavigationController(rootViewController: CalendarScene.build())
        } else {
            self.window?.rootViewController = UINavigationController(rootViewController: CalendarScene.build())
        }
        self.window?.makeKeyAndVisible()
    }
}
